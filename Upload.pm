package CC::Upload;
use strict;
use warnings;
use Exporter 'import';
our @EXPORT_OK = qw(upload upload_notify);
use Apache2::Upload;
use CC::IO qw/render/;
use CC::File qw(sanitize);

sub upload {  #{{{
    my ($r, $arglist, $session, $dbh, $STH, $CONFIG, $USR) = @_;
    my $dir = exists $USR->{'perms'}{'see_all'}
        ? "$CONFIG->{'filedir'}/"
        : "$CONFIG->{'filedir'}/$USR->{'user'}/";
    my $err = '';
    my $req = exists $USR->{'POST_MAX'}
            ? Apache2::Request->new($r, POST_MAX => $USR->{'POST_MAX'})
            : Apache2::Request->new($r);
    my $status = $req->body_status();
    if ( $status == 0 ) {
        $dir .= $req->param('xdir');
        if (my $upload = $req->upload("upload")) {
            my $filename = $upload->filename();
            $filename =~ s/.*[\/\\]//;
            $filename = sanitize($filename, $CONFIG->{'utf8'});
            $upload->link("$dir/$filename") or
                $err = "?Upload failed: $!";
            upload_notify($USR, "$dir/$filename")
                if exists $USR->{'notify'};
        }
        $r->headers_out->set(Location =>
            "$CONFIG->{'rooturi'}/" . $req->param('xdir') . $err);
    } else {
        $r->headers_out->set(Location =>
            "$CONFIG->{'rooturi'}/?Upload Error: $status");
    }
    $r->status(Apache2::Const::REDIRECT);
    return Apache2::Const::REDIRECT;
}
#}}}
sub upload_notify {  #{{{
    my ($USR, $file) = @_;
    open (MAIL, "|/usr/sbin/sendmail $USR->{'notify'}");
    print MAIL "Subject: Upload Notification\n";
    print MAIL "From: Codefix Consulting <garrison\@codefix.net>\n";
    print MAIL "To: $USR->{'notify'}\n\n";
    print MAIL "A user has uploaded a file:\n\n";
    print MAIL "USER: $USR->{'user'}\n";
    print MAIL "FILE: $file\n";
    close (MAIL);
}
#}}}
1;
