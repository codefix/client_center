package CC::File;
use strict;
use warnings;
use Exporter 'import';
our @EXPORT_OK = qw(get update rm mk_dir show ls sanitize zip);
use Apache2::SubRequest ();
use Apache2::RequestIO ();
use File::Path;
use File::stat;
use File::Copy::Recursive qw(dirmove);
use CC::IO qw/render/;
use Archive::Zip qw( :ERROR_CODES :CONSTANTS );

sub zip {  #{{{
    my ($r, $arglist, $session, $dbh, $STH, $CONFIG, $USR) = @_;
    my $home = exists $USR->{'perms'}{'see_all'}
        ? "$CONFIG->{'filedir'}/"
        : "$CONFIG->{'filedir'}/$USR->{'user'}/";
    my $path = $r->uri;
    $path =~ s/$CONFIG->{'rooturi'}\/zip\/?/$home/;
    # Enforce zip_top restriction. 
    if (($path eq $home) && (!exists $USR->{'perms'}{'zip_top'})) {
        my $redirect = "$CONFIG->{'rooturi'}/?Error: Zip Access Denied.";
        $r->headers_out->set(Location => $redirect);
        $r->status(Apache2::Const::REDIRECT);
        return Apache2::Const::REDIRECT;
    }
    
    my $file = $path;
    $file =~ s/$CONFIG->{'filedir'}//;
    my $zip = Archive::Zip->new();
    if (-d $path) {
        $zip->addTree( $path, '' );
    }
    elsif (-f $path) {
        $zip->addFile( $path, $file );
    }
    else {
        warn "Can't access $path";
        return Apache2::Const::OK;
    }
    $file =~ s/\/*$/.zip/;
    $file =~ s/^.*\///;
    $file = 'folder.zip' if $file eq '.zip';
    $r->headers_out->{'Content-Disposition'} = "attachment;filename=\"$file\"";
    $r->content_type('application/zip');
    tie *ZIP => $r;  # Tie to the Apache::RequestRec object
    binmode(*ZIP);
    $zip->writeToFileHandle( \*ZIP );
    return Apache2::Const::OK;
}
#}}}
sub get {  #{{{
    my ($r, $arglist, $session, $dbh, $STH, $CONFIG, $USR) = @_;
    my $uri = $r->uri;
    my $path = $uri;
    $path =~ s/$CONFIG->{'rooturi'}\/get/$CONFIG->{'filedir'}/;
    if ((-d $path) || (! -e $path)) { # Apache discloses real URI
        my $redirect = "$CONFIG->{'rooturi'}/?Error: Bad Request.";
        $r->headers_out->set(Location => $redirect);
        $r->status(Apache2::Const::REDIRECT);
        return Apache2::Const::REDIRECT;
    }
    $uri =~ s/$CONFIG->{'rooturi'}\/get/$CONFIG->{'fileuri'}/;
    $uri =~ s/([^A-Za-z0-9_\/])/sprintf("%%%02X", ord($1))/seg;
    $r->internal_redirect($uri);
    return Apache2::Const::OK;
}
#}}}
sub update {  #{{{
    my ($r, $arglist, $session, $dbh, $STH, $CONFIG, $USR) = @_;
    my $req = Apache2::Request->new($r);
    
    my (%perm, $err);
    for (qw/staff admin client/) {
        $perm{$_} = defined $req->param($_) ? 1 : 0;
    }
    
    my $user = sanitize($req->param('user'), $CONFIG->{'utf8'});
    my $name = $req->param('name');
    $user = 'Unknown' unless length($user);
    $name = 'Unknown' unless length($name);
    my $xuser = $req->param('xuser');

    my $uid = $req->param('id');
    $uid = 0 unless defined $uid;

    my $dir = "$CONFIG->{'filedir'}/" . $user;
    my $oldir = $xuser eq '' ? ''
                             : "$CONFIG->{'filedir'}/" . $req->param('xuser');
    if ((! -e $dir) && ($req->param('action') ne 'Delete')) {
        if (-e $oldir) {
            dirmove($oldir,$dir)
                or $err = "?Cannot move $oldir $!";
        } else {
            mkdir("$dir") or $err = "?$!";
        }
    } elsif (
        (-d $dir)
        && ($req->param('action') eq 'Delete')
        && ($uid > 0)
    ) {
        rmtree("$dir") or $err = "?$!";
    }

    if (
        (not defined $err)
        && ($uid == 0)
        && ($req->param('action') eq 'Submit')
    ) {
        $STH->{'INSERT'}->execute(
                            $user,
                            $req->param('pass'),
                            $name,
                            $perm{'staff'},
                            $perm{'admin'},
                            $perm{'client'},
                            $req->param('notify')||'',
        ) or $err = '?Error: ' . $STH->{'INSERT'}->errstr;
    } elsif (
        (not defined $err)
        && ($uid =~ /^\d+$/)
        && ($req->param('action') eq 'Submit')
    ) {
        $STH->{'UPDATE'}->execute(
                            $user,
                            $req->param('pass'),
                            $name,
                            $perm{'staff'},
                            $perm{'admin'},
                            $perm{'client'},
                            $req->param('notify')||'',
                            $uid,
        ) or $err = '?Error: ' . $STH->{'UPDATE'}->errstr;
    } elsif (
        (not defined $err)
        && ($uid =~ /^\d+$/)
        && ($req->param('action') eq 'Delete')
    ) {
        $STH->{'DELETE'}->execute($uid)
            or $err = '?Error: ' . $STH->{'UPDATE'}->errstr;
    }
    
    my $redirect = "$CONFIG->{'rooturi'}/admin/";
    $redirect .= $err unless not defined $err;
    $r->headers_out->set(Location => $redirect);
    $r->status(Apache2::Const::REDIRECT);
    return Apache2::Const::REDIRECT;
}
#}}}
sub rm {  #{{{
    my ($r, $arglist, $session, $dbh, $STH, $CONFIG, $USR) = @_;
    # Sanitize input, chroot
    shift @$arglist;
    map { s/\.\.\///g; } @$arglist;
    my $goner = exists $USR->{'perms'}{'see_all'}
        ? "$CONFIG->{'filedir'}/"
        : "$CONFIG->{'filedir'}/$USR->{'user'}/";

    my $xdir = join('/', @$arglist);
    $goner .= $xdir;
    $xdir =~ s/[^\/]+\/?$//;

    my $err = '';
    if ((-d $goner) && ($USR->{'perms'}{'rm_dir'} == 1)) {
        rmtree($goner) or $err = "?$!";
    }
    if ((-f $goner) && ($USR->{'perms'}{'rm_file'} == 1)) {
        unlink("$goner") or $err = "?$!";
    }

    $r->headers_out->set(Location =>
        "$CONFIG->{'rooturi'}/$xdir" . $err);
    $r->status(Apache2::Const::REDIRECT);
    return Apache2::Const::REDIRECT;
}
#}}}
sub mk_dir {  #{{{
    my ($r, $arglist, $session, $dbh, $STH, $CONFIG, $USR) = @_;
    my $req = Apache2::Request->new($r);
    my $dir = exists $USR->{'perms'}{'see_all'}
        ? "$CONFIG->{'filedir'}/"
        : "$CONFIG->{'filedir'}/$USR->{'user'}/";
    $dir .= $req->param('xdir');
    my $err = '';
    if (-d $dir) {
        my $folder = sanitize($req->param('folder'), $CONFIG->{'utf8'});
        mkdir("$dir/$folder") or $err = "?$!";
    }
    $r->headers_out->set(Location =>
        "$CONFIG->{'rooturi'}/" . $req->param('xdir') . $err);
    $r->status(Apache2::Const::REDIRECT);
    return Apache2::Const::REDIRECT;
}
#}}}
sub show {  #{{{
    my ($arglist, $CONFIG, $USR) = @_;
    # Sanitize input, chroot
    map { sanitize($_, $CONFIG->{'utf8'}) } @$arglist;
    my $home = exists $USR->{'perms'}{'see_all'}
        ? "$CONFIG->{'filedir'}/"
        : "$CONFIG->{'filedir'}/$USR->{'user'}/";

    $USR->{'iconuri'} = $CONFIG->{'iconuri'};
    $USR->{'xdir'} = join('/', @$arglist);

    if (($#$arglist < 0) && (exists $USR->{'perms'}{'see_all'})) {
        delete $USR->{'perms'}{'upload'};
        delete $USR->{'perms'}{'mk_dir'};
        delete $USR->{'perms'}{'rm_dir'};
    }

    # Zip icon.
    if (exists $USR->{'perms'}{'zip'} || $USR->{'perms'}{'zip_top'}) {
        if (exists $USR->{'perms'}{'zip_top'} or length($USR->{'xdir'})) {
            $USR->{'zip'} = sprintf(
                '<a href="%s" title="%s"><img src="%s" border=0></a>',
                "$CONFIG->{'rooturi'}/zip/$USR->{'xdir'}",
                'Download this folder as a zip archive',
                $USR->{'iconuri'} . $CONFIG->{'icons'}{'zip'},
            );
        }
    }
    
    my $dir = $home . $USR->{'xdir'};
    if (-d $dir){
        my $cur = "$CONFIG->{'rooturi'}/";
        map {
            $cur .= "$_/";
            $USR->{'current'} .= "<a href='$cur'>$_</a>/";
        } @$arglist;

        $USR->{'files'} = ls($home, $CONFIG, $USR);
    } else {
        $USR->{'error'} = "Invalid folder.";
    }
    render($CONFIG, 'files.html', $USR);
}
#}}}
sub ls {  #{{{
    my ($home, $CONFIG, $USR) = @_;
    my ($ext, %dir);
    my $realdir = $home . $USR->{'xdir'};
    $realdir =~ s/\/\//\//g;
    $realdir =~ s/\/+$//;
    my $dload = $realdir;
    my $chdir = $realdir;
    my $rmuri = $realdir;
    opendir(DIR, $realdir);
    $dload =~ s/$CONFIG->{'filedir'}/$CONFIG->{'rooturi'}\/get/;
    $chdir =~ s/$home?/$CONFIG->{'rooturi'}\//
        or die "Huh? $rmuri - $home :: $USR->{'xdir'} :: $CONFIG->{'rooturi'}";
    $rmuri =~ s/$home?/$CONFIG->{'rooturi'}\/rm\//;
    $chdir =~ s/\/+$//;
    $rmuri =~ s/\/+$//;
    my $updir = $chdir;
    $updir =~ s/\/[^\/]+$//;
    while (my $file = readdir(DIR)) {
        next if $file eq '.';
        my $urifile = $file;
        $urifile =~ s/([^A-Za-z0-9_\/])/sprintf("%%%02X", ord($1))/seg;
        
        my $rmlink = "<a href=\"$rmuri/$urifile\" ";
        $rmlink .= "OnClick=\"del = confirm('Are you sure you wish to delete ";
        $rmlink .= "$file? \\nThis action cannot be undone.'); return del\">";
           $rmlink .= "<img src='$CONFIG->{'iconuri'}";
           $rmlink .= "$CONFIG->{'icons'}{'del'}' border=0></a>";
        if ($file =~ /.*\.([A-z0-9]+)$/) {
            $ext = lc($1);
        }
        else {
            $ext = 'unk';
        }
        if (-f "$home$USR->{'xdir'}/$file") {
            my $sb = stat("$home$USR->{'xdir'}/$file")
                or die "Cannot stat $file: $!";
            $dir{$file}{'size'} = nicebytes($sb->size);
            $dir{$file}{'mtime'} = scalar localtime $sb->mtime;
            $dir{$file}{'img'} = exists $CONFIG->{'icons'}{$ext}
                                ? $CONFIG->{'icons'}{$ext}
                                : $CONFIG->{'icons'}{'unk'};
            $dir{$file}{'lnk'} = "$dload/$urifile";
            $dir{$file}{'rm'} = $rmlink if exists $USR->{'perms'}{'rm_file'};
        } else {
            if ($file eq '..') {
                next unless length($updir);
                $dir{$file}{'img'} = $CONFIG->{'icons'}{'updir'};
                $dir{$file}{'lnk'} = "$updir/";
            } else {
                $dir{$file}{'img'} = $CONFIG->{'icons'}{'dir'};
                $dir{$file}{'lnk'} = "$chdir/$urifile/";
                $dir{$file}{'rm'} = $rmlink if exists $USR->{'perms'}{'rm_dir'};
            }
        }
    }
    return \%dir;
    closedir(DIR);
}
#}}}
sub sanitize {  #{{{
    my ($input, $utf8) = @_;
    $input =~ s/\s+/ /g;
    $input =~ s/\s+$//g;
    $input =~ s/^\s+//g;
    $input =~ s/[\/]/_/g;
    $input =~ s/[[:^ascii:]]/_/g unless defined $utf8;
    return $input;
}
#}}}
sub nicebytes {  #{{{
    my $b = shift;
    if ($b < 1024)   { return sprintf('%d Bytes', $b); }
    if ($b < (1024*1024)) { return sprintf('%.2f KB', $b/1024); }
    return sprintf('%.2f MB', $b/1024/1024);
}
#}}}
