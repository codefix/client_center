package CC::Core;
use strict;
use warnings;
use Config::YAML;
use Apache2::Const -compile => ':common';
use Apache2::Directive ();
use CC::Auth qw/login logout/;
use CC::DB qw(load_sql get_user admin);
use CC::File qw(get update rm mk_dir show ls zip);
use CC::IO qw/get_session render/;
use CC::Upload qw/upload upload_notify/;

my %ACTIONS = (  #{{{
    'login'  => \&login,
    'logout' => \&logout,
    'mk_dir' => \&mk_dir,
    'upload' => \&upload,
    'update' => \&update,
    'rm'     => \&rm,
    'admin'  => \&admin,
    'get'    => \&get,
    'zip'    => \&zip,
);
#}}}
sub handler {  #{{{
    my $r = shift;
    my $uri = $r->uri;
    my $docroot = $r->document_root;

    my $CONFIG = Config::YAML->new(config => "$docroot/config.yaml")
        or die "No usable config files found!";

    $uri =~ s/$CONFIG->{'rooturi'}\/?//;
    my @arglist = split('/', $uri);

    ### $dbh and prepared SQL handles ###
    my ($dbh, $STH) = load_sql($CONFIG->{'db'})
          or die "Couldn't connect to database: $!";
    
    ### Session, cookies ###
    my ($cookie_id, $session, $cookies ) = get_session(
                                               $r, $dbh, $STH, $CONFIG
                                           );
    ### Check Auth ###
    if ((defined $arglist[0]) && ($arglist[0] eq 'login')) {
        return &login($r, \@arglist, $session, $dbh, $STH, $CONFIG, undef);
    }

    ### Get User Data or kill session ###
    my $USR = get_user($r, $session, $STH, $CONFIG) or tied(%$session)->delete;

    $r->content_type('text/html');
    return &render($CONFIG, 'login.html') unless exists $session->{'uid'};

    ### Call Dispatch Table ###
    my $dispatch = $arglist[0] || '';
    if (exists $ACTIONS{$dispatch}) {
        return &{ $ACTIONS{$dispatch} }(
            $r, \@arglist, $session, $dbh, $STH, $CONFIG, $USR
        );
    }
    else {
        return &show(\@arglist, $CONFIG, $USR);
    }

    return Apache2::Const::OK;
}
#}}}
1;
