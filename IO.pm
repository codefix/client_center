package CC::IO;
use strict;
use warnings;
use Exporter 'import';
our @EXPORT_OK = qw(get_session render);
use Apache::Session::MySQL;
use Apache2::Cookie;
use Text::Template 'fill_in_string';

sub get_session {  #{{{
    my ($r, $dbh, $STH, $CONFIG) = @_;
    my ($cookie_id, %session, %cookies);
    if(%cookies = Apache2::Cookie->fetch($r)){
        # In some b0rked browsers, $cookies{'id'} is undef here.
        $cookie_id = defined $cookies{'ftp_id'} ? $cookies{'ftp_id'}->value
                                                : undef;
    }
    $cookie_id = undef
        unless (defined $cookie_id && ($cookie_id =~ /^[a-fA-F0-9]+$/));

    eval {
        tie %session, 'Apache::Session::MySQL', $cookie_id, {
            Handle     => $dbh,
            LockHandle => $dbh
        };
    };
    if ($@) {  # Whoops! Old session is b0rked, make a new one.
        tie %session, 'Apache::Session::MySQL', undef, {
            Handle     => $dbh,
            LockHandle => $dbh
        };
    };
    my $session_id = $session{_session_id};
    $STH->{'SESSION'}->execute($ENV{'REMOTE_ADDR'}, $session_id)
        or die "Couldn't execute statement: " . $STH->{'SESSION'}->errstr;

    ### Cookie Stuff
    my $cookie = Apache2::Cookie->new($r,
                        -name    => "ftp_id",
                        -value   => $session_id,
                        -expires => $CONFIG->{'timeout'},
                        -path    => '/',
                 );
    $cookie->bake($r);
    return ($cookie_id, \%session, \%cookies);
}
#}}}
sub render {  #{{{
    my ($CONFIG, $page, $VARS) = @_;
    $VARS->{'rooturi'} = $CONFIG->{'rooturi'};
    print build("$CONFIG->{'tmpldir'}/header.html");
    print build("$CONFIG->{'tmpldir'}/$page", $VARS);
    print build("$CONFIG->{'tmpldir'}/footer.html");
    return Apache2::Const::OK;
}
#}}}
sub build {  #{{{
    my ($template_file, $vars) = @_;
    my $template = Text::Template->new(
                    DELIMITERS => ['<?', '?>'],
                    SOURCE     => "$template_file",
    ) or die "Couldn't construct template: $Text::Template::ERROR";
    return $template->fill_in(HASH => $vars);
}
#}}}
sub decode_url {  #{{{
    my( $url ) = @_;
    $url =~ s/\+/ /g;
    $url =~ s/%([a-fA-F0-9][a-fA-F0-9])/chr hex $1/eg;
    return $url;
}
#}}}
