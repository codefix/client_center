package CC::DB;
use strict;
use warnings;
use DBI;
use Exporter 'import';
our @EXPORT_OK = qw(load_sql get_user admin);
use CC::IO qw/render/;

my %SQL = ( #{{{
    SESSION => q{
        UPDATE sessions SET ip = ?
        WHERE id = ?
    },

    LIST => q{
        SELECT id, user, pass, name, staff, admin, client
        FROM cc_users
        ORDER BY user;
    },

    USER => q{
        SELECT id, user, pass, name, staff, admin, client, notify
        FROM cc_users
        WHERE id = ?
    },

    LOGIN => q{
        SELECT id, user, pass, name, staff, admin, client, notify
        FROM cc_users
        WHERE user = ? AND pass = ?
    },

    INSERT => q{
        INSERT INTO cc_users
        (user, pass, name, staff, admin, client, notify)
        VALUES (?, ?, ?, ?, ?, ?, ?);
    },

    UPDATE => q{
        UPDATE cc_users
        SET user = ?, pass = ?, name = ?, staff = ?,
            admin = ?, client = ?, notify = ?
        WHERE id = ?;
    },

    DELETE => q{
        DELETE from cc_users
        WHERE id = ?;
    },
);
#}}}
sub load_sql {  #{{{
    my $db = shift;
    my $dbh = DBI->connect($db->{'dsn'}, $db->{'user'}, $db->{'pass'})
        or die "Couldn't connect to database: " . DBI->errstr;
    my %STH;
    for ( keys %SQL ) {
        $STH{$_} = $dbh->prepare($SQL{$_}) or die $dbh->errstr;
    }
    return ($dbh, \%STH);
}
#}}}
sub get_user {  #{{{
    my ($r, $session, $STH, $CONFIG) = @_;
    my ($USR);
    $STH->{'USER'}->execute($session->{'uid'}) or die $STH->{'USER'}->errstr;

    # Good session, bad user
    return undef unless $STH->{'USER'}->rows;

    $USR = $STH->{'USER'}->fetchrow_hashref();
    $USR->{'anyone'} = 1;
    for my $role (qw/anyone client staff admin/) {
        if ($USR->{$role} == 1) {
            map { $USR->{'perms'}{$_} = 1; } @{ $CONFIG->{$role} }
        }
    }
    # Use the highest available role for POST_MAX
    for my $role (qw/admin staff client anyone/) {
        if ($USR->{$role} == 1) {
            if (exists $CONFIG->{'POST_MAX'}->{$role}) {
                $USR->{'POST_MAX'} = eval $CONFIG->{'POST_MAX'}->{$role};
            }
            last;
        }
    }
    $USR->{'error'} = $r->args() || '';
    $USR->{'error'} =~ s/%20/ /g;

    # This should probably use Email::Validate
    if (exists $USR->{'notify'} && length($USR->{'notify'}) < 5) {
        delete $USR->{'notify'};
    }
    return $USR;
}
#}}}
sub admin {  #{{{
    my ($r, $arglist, $session, $dbh, $STH, $CONFIG, $USR) = @_;
    my $page = 'admin.html';
    return render($CONFIG, 'error.html', $USR) unless $USR->{'admin'};
    if ((defined $arglist->[1]) && ($arglist->[1] == 0)) {
        $page = 'edit.html';
    } elsif ((defined $arglist->[1]) && ($arglist->[1] =~ /^\d+$/)) {
        $STH->{'USER'}->execute($arglist->[1]) or die $STH->{'USER'}->errstr;
        $USR->{'u'} = $STH->{'USER'}->fetchrow_hashref();
        $page = 'edit.html';
    } else {
        $STH->{'LIST'}->execute() or die $STH->{'LIST'}->errstr;
        $USR->{'rows'} = $STH->{'LIST'}->rows;
        $USR->{'users'} = $STH->{'LIST'}->fetchall_arrayref({});
    }
    $USR->{'error'} = $r->args() || '';
    $USR->{'error'} =~ s/%20/ /g;
    return render($CONFIG, $page, $USR);
}
#}}}
