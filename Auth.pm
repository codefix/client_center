package CC::Auth;
use strict;
use warnings;
use Exporter 'import';
our @EXPORT_OK = qw(login logout);
use CC::IO qw/render/;

sub login {  #{{{
    my ($r, $arglist, $session, $dbh, $STH, $CONFIG, $USR) = @_;
    my $req = Apache2::Request->new($r);
    $STH->{'LOGIN'}->execute($req->param('login'), $req->param('passwd'))
        or die $STH->{'LOGIN'}->errstr;
    $USR = $STH->{'LOGIN'}->fetchrow_hashref();
    if (exists $USR->{'id'}) {
        $session->{'uid'} = $USR->{'id'};
        $r->headers_out->set(Location => $CONFIG->{'rooturi'});
        $r->status(Apache2::Const::REDIRECT);
        return Apache2::Const::REDIRECT;
    } else {
        $r->content_type('text/html');
        return &render($CONFIG, 'login.html', $USR);
    }
}
#}}}
sub logout {  #{{{
    my ($r, $arglist, $session, $dbh, $STH, $CONFIG, $USR) = @_;
    tied(%$session)->delete;
    $r->content_type('text/html');
    return &render($CONFIG, 'login.html', $USR);
}
#}}}
