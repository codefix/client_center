#!/usr/bin/perl
use strict;
use warnings;

require 5.008;
my %modules = (
    'Apache::Session::MySQL' => 'libapache-session-perl',
    'Apache2::Upload'        => 'libapache2-request-perl',
    'Date::Manip'            => 'libdate-manip-perl',
    'Text::Template'         => 'libtext-template-perl',
    'YAML'                   => 'libyaml-perl',

    'Config::YAML'           => 'libconfig-yaml-perl',
    'File::Copy::Recursive'  => 'libfile-copy-recursive-perl',
);
my %missing;
map {
    eval "require $_";
    $missing{$_} = $modules{$_} if $@;
} sort keys %modules;

if (keys %missing) {
    my $str = "Needed modules:\n\t%s\n\n";
    $str .= "Install on Debian/Ubuntu with:\n\tsudo apt-get install %s\n",
    printf $str, join("\n\t", keys %missing), join(' ', values %missing);
}
else {
    print "Ok\n";
}
